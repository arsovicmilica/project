package com.entity;

import lombok.Data;


import javax.persistence.*;

@Entity
@Data
@Table(name = "user")

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String pageUrl;

    @Column
    private String pageImpressions;

    @Column
    private String clicks;

    @Column
    private String orders;

    @Column
    private String orderValue;

    @Column
    private String publisherOrderRevenue;

    @Column
    private String cpcClicks;

    @Column
    private  String publisherCpcRevenue;

    @Column
    private String eCPC;

    @Column
    private String commissionRate;

    @Column
    private String totalEarnings;

    @Column
    private String author;


}
