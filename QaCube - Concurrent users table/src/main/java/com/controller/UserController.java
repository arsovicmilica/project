package com.controller;

import com.dto.UserDTO;
import com.entity.User;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


@RestController
@RequestMapping(value = "/user")
@CrossOrigin("http://localhost:4200")
public class UserController {

    Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public UserDTO getUsers(@PathVariable Long id) {
        this.logger.info("Get user data by id");
        return this.userService.getUserById(id);
    }

    @GetMapping
    public Page<User> getAllEmployees(@RequestParam int pageSize, @RequestParam int pageNumber) {
        Page<User> list = this.userService.getAllUsers(pageNumber, pageSize);
        return list;
    }

    @GetMapping("/search")
    public Page<User> search(@RequestParam String text, @RequestParam int pageSize, @RequestParam int pageNumber){
        return this.userService.search(pageNumber, pageSize, text);
    }

    @PostMapping()
    public UserDTO save(@RequestBody UserDTO userDTO) {
        this.logger.info("Save user");
        return  this.userService.save(userDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteUser(@PathVariable Long id) {
        this.logger.info("Delete user");
        this.userService.deleteUser(id);
    }

    @PutMapping()
    public UserDTO updateUser(@RequestBody UserDTO userDTO) {
        this.logger.info("Update user");
        return this.userService.updateUser(userDTO);
    }

}
