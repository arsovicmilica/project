package com.service;


import com.dto.UserDTO;
import com.entity.User;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {



    UserDTO getUserById(Long id);

    UserDTO updateUser(UserDTO user);

    void deleteUser(Long id);

    User getUserByPageUrl(String pageUrl);

    UserDTO save(UserDTO user);

    UserDTO mapToDTO(User user);

    Page<User> getAllUsers(Integer pageNo, Integer pageSize);

    Page<User> search(int pageNumber, int pageSize, String text);
}