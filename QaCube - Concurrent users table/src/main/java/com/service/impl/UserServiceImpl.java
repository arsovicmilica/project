package com.service.imp;

import com.dto.UserDTO;
import com.entity.User;
import com.repository.UserRepository;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    public Page<User> getAllUsers(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of(pageNo, pageSize);

        Page<User> pagedResult = userRepository.findAll(paging);

        return pagedResult;
    }

    @Override
    public Page<User> search(int pageNumber, int pageSize, String text) {
        Pageable paging = PageRequest.of(pageNumber, pageSize);

        Page<User> pagedResult = userRepository.findAllByPageUrl(text, paging);

        return pagedResult;
    }

    @Override
    public UserDTO getUserById(Long id) {
        return this.mapToDTO(this.userRepository.findUserById(id));
    }

    @Override
    public UserDTO updateUser(UserDTO userDTO) {
        User korisnik = this.mapToEntity(getUserById(userDTO.getId()));
        korisnik.setPageUrl(userDTO.getPageUrl());
        korisnik.setPageImpressions(userDTO.getPageImpressions());
        korisnik.setClicks(userDTO.getClicks());
        korisnik.setOrders(userDTO.getOrders());
        korisnik.setOrderValue(userDTO.getOrderValue());
        korisnik.setPublisherOrderRevenue(userDTO.getPublisherOrderRevenue());
        korisnik.setCpcClicks(userDTO.getCpcClicks());
        korisnik.setPublisherCpcRevenue(userDTO.getPublisherCpcRevenue());
        korisnik.setECPC(userDTO.getECPC());
        korisnik.setCommissionRate(userDTO.getCommissionRate());
        korisnik.setTotalEarnings(userDTO.getTotalEarnings());
        korisnik.setAuthor(userDTO.getAuthor());
        UserDTO sacuvaniUser = save(this.mapToDTO(korisnik));
        return sacuvaniUser;
    }

    @Override
    public void deleteUser(Long id) {
        User korisnik = this.mapToEntity(getUserById(id));
        this.userRepository.delete(korisnik);
    }

    @Override
    public User getUserByPageUrl(String pageUrl) {
        return null;
    }

    @Override
    public UserDTO save(UserDTO dto) {
        User savedUser = this.userRepository.save(this.mapToEntity(dto));
        return this.mapToDTO(savedUser);
    }

    @Override
    public UserDTO mapToDTO(User user) {
        UserDTO dto = new UserDTO(user);
        return dto;
    }

    public User mapToEntity(UserDTO dto) {
        User user = new User();
        user.setPageUrl(dto.getPageUrl());
        user.setPageImpressions(dto.getPageImpressions());
        user.setClicks(dto.getClicks());
        user.setOrders(dto.getOrders());
        user.setOrderValue(dto.getOrderValue());
        user.setPublisherOrderRevenue(dto.getPublisherOrderRevenue());
        user.setCpcClicks(dto.getCpcClicks());
        user.setPublisherCpcRevenue(dto.getPublisherCpcRevenue());
        user.setECPC(dto.getECPC());
        user.setCommissionRate(dto.getCommissionRate());
        user.setTotalEarnings(dto.getTotalEarnings());
        user.setAuthor(dto.getAuthor());
        return user;
    }
}