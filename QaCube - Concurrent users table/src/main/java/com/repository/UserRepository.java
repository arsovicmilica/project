package com.repository;

import com.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


@Repository
public interface UserRepository  extends PagingAndSortingRepository<User, Long> {


   User findUserByPageUrl(String pageUrl);

   User findUserById(Long id);

   Page<User> findAll(Pageable pageable);

   @Query(value = "select u from User u where lower(u.pageUrl) like concat('%', lower(:pageUrl), '%' )")
   Page<User> findAllByPageUrl(String pageUrl,Pageable pageable);

}
