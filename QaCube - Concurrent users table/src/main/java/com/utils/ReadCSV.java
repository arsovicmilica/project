package com.utils;

import com.dto.UserDTO;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Component
public class ReadCSV implements CommandLineRunner {

    @Autowired
    public UserService userService;

    @Override
    public void run(String... args) throws Exception {

        String path = "C:\\Users\\sixtest01\\Downloads\\user.csv";

        String line = " ";

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            while ((line = br.readLine()) != null) {
                System.out.println(line);
                String[] values = line.split(",");
                UserDTO user = new UserDTO();
                if (0 <= values.length - 1) {
                    user.setPageUrl(values[0]);
                }
                if (1 <= values.length - 1)
                    user.setPageImpressions(values[1]);
                if (2 <= values.length - 1)
                    user.setClicks(values[2]);
                if (3 <= values.length - 1)
                    user.setOrders(values[3]);
                if (4 <= values.length - 1)
                    user.setOrderValue(values[4]);
                if (5 <= values.length - 1)
                    user.setPublisherOrderRevenue(values[5]);
                if (6 <= values.length - 1)
                    user.setCpcClicks(values[6]);
               if (7 <= values.length - 1)
                    user.setPublisherCpcRevenue(values[7]);
               if (8 <= values.length - 1)
                    user.setECPC(values[8]);
                if (9 <= values.length - 1)
                   user.setCommissionRate(values[9]);
               if (10 <= values.length - 1)
                    user.setTotalEarnings(values[10]);
                if (11 <= values.length - 1)
                    user.setAuthor(values[11]);

                this.userService.save(user);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
        }
    }
}
