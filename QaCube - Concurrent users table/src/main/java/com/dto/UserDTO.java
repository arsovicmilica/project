package com.dto;

import com.entity.User;
import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String pageUrl;
    private String pageImpressions;
    private String clicks;
    private String orders;
    private String orderValue;
    private String publisherOrderRevenue;
    private String cpcClicks;
    private String publisherCpcRevenue;
    private String eCPC;
    private String commissionRate;
    private String totalEarnings;
    private String author;

    public UserDTO(User user) {
        this.id = user.getId();
        this.pageUrl = user.getPageUrl();
        this.pageImpressions = user.getPageImpressions();
        this.clicks = user.getClicks();
        this.orders = user.getOrders();
        this.orderValue = user.getOrderValue();
        this.publisherOrderRevenue = user.getPublisherOrderRevenue();
        this.cpcClicks = user.getCpcClicks();
        this.publisherCpcRevenue = user.getPublisherCpcRevenue();
        this.eCPC = user.getECPC();
        this.commissionRate = user.getCommissionRate();
        this.totalEarnings = user.getTotalEarnings();
        this.author = user.getAuthor();
}

    public UserDTO() {

    }
    }
