export class User {


  public pageUrl!: string;
  public pageImpressions!: string;
  public clicks!: string;
  public orders!: string;
  public orderValue!: string;
  public publisherOrderRevenue!: string;
  public cpcClicks!: string;
  public publisherCpcRevenue!: string;
  public eCPC!: string;
  public commissionRate!: string;
  public totalEarnings!: string;
  public author!: string;
}
