import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private http: HttpClient) { }

  getAll(pageNum: number, pageSize: number): Observable<any> {
    return this.http.get('http://localhost:8080/user?pageNumber=' + pageNum +'&pageSize=' + pageSize);
  }

  search(pageNum: number, pageSize: number, text: string): Observable<any> {
    return this.http.get('http://localhost:8080/user/search?pageNumber=' + pageNum +'&pageSize=' + pageSize + '&text=' + text);
  }


  getById(userId: number): any {
    return this.http.get("http://localhost:8080/user/" + userId);
  }
}
