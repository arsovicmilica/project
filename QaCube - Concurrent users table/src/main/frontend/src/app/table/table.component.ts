import { Component, OnInit } from '@angular/core';
import {TableService} from "./table.service";
import {User} from "./table.model";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  offset: number = 10;
  limit: number = 10;
  size: number = 100;

  filterString = "";
  users: User[] = [];
  private result: any;

  page: number = 1;
  total: number = 0;
  itemsPerPage: number = 50;
  text = '';

  constructor(private tableService: TableService) { }

  ngOnInit(): void {
    this.tableService.getAll(this.page - 1, 50).subscribe((response: any) => {
      this.users = response.content;
      this.total = response.totalElements;
      this.page = response.number + 1;
    });
    this.onFilterChange();
  }

  onFilterChange() {
    this.users = this.users.filter((user) => this.isMatch(user));
  }

  isMatch(item: any): any {
    if (item instanceof Object) {
      return Object.keys(item).some((k) => this.isMatch(item[k]));
    } else {
      return item != null ? item.toString().indexOf(this.filterString) > -1 : null;
    }
  }

  onPageChanged(number: number) {
    this.page = number;
    this.tableService.getAll(number - 1, 50).subscribe((response: any) => {
      this.users = response.content;
      this.total = response.totalElements;
    });
  }

  search() {
    this.tableService.search(this.page - 1, 50, this.text).subscribe((response: any) => {
      this.users = response.content;
      this.total = response.totalElements;
    })
  }
  key:string = 'id';
  reverse:boolean = false;
  sort(key : any){
    this.key= key;
    this.reverse = !this.reverse;
  }

}
